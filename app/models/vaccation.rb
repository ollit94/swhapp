class Vaccation < ApplicationRecord
  belongs_to :player
  validates :description, presence: true, length: {minimum: 3, maximum: 200}

  def get_from
    self.from_date
  end

  def get_until
    self.until_date
  end

  def get_reason
    self.description
  end
end
