class PenaltiesController < ApplicationController
  before_action :require_kassierer_or_admin_or_trainer, except: [:index]
  before_action :set_penalty, only: [:edit, :update, :destroy]

  def index
    @penalties = Penalty.order(:name).paginate(page: params[:page], per_page: 10)
  end

  def new
    @penalty = Penalty.new
  end

  def create
    @penalty = Penalty.new(penalty_params)
    if @penalty.save
      flash[:success] = "Strafe erfolgreich angelegt"
      redirect_to penalties_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @penalty.update_attributes(penalty_params)
      flash[:success] = "Stafe erfolgreich geändert"
      redirect_to penalties_path
    else
      render 'edit'
    end
  end

  def destroy
    @penalty.destroy
    flash[:success] = "Strafe erfolgreich gelöscht"
    redirect_to penalties_path
  end

  private
    def penalty_params
      params.require(:penalty).permit(:costs,:name)
    end

    def set_penalty
      @penalty = Penalty.find(params[:id])
    end
end
