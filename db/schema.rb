# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190116142400) do

  create_table "away_games", force: :cascade do |t|
    t.datetime "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cancellations", force: :cascade do |t|
    t.datetime "from"
    t.datetime "until"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "player_id"
  end

  create_table "cards", force: :cascade do |t|
    t.integer "player_id"
    t.datetime "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "deposits", force: :cascade do |t|
    t.integer "player_id"
    t.datetime "date"
    t.decimal "amount", precision: 8, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "away_game_id", default: 0
  end

  create_table "money_transcations", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "penalties", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.float "costs"
  end

  create_table "player_away_games", force: :cascade do |t|
    t.integer "player_id"
    t.integer "away_game_id"
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "player_of_the_games", force: :cascade do |t|
    t.integer "player_id"
    t.datetime "date"
    t.string "gegner"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "player_penalties", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "player_id"
    t.integer "penalty_id"
    t.datetime "date"
    t.integer "away_game_id"
  end

  create_table "players", force: :cascade do |t|
    t.string "firstname"
    t.string "secondname"
    t.string "username"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "remember_digest"
    t.integer "training_possible", default: 0
    t.integer "training_not_present", default: 0
    t.string "role", default: "0"
    t.string "image"
    t.decimal "amount", precision: 8, scale: 2, default: "0.0"
    t.integer "num_cards", default: 0
    t.integer "num_potg", default: 0
    t.boolean "active", default: true
  end

  create_table "training_sessions", force: :cascade do |t|
    t.datetime "date_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "place", default: "Hultrop"
  end

  create_table "training_sessions_players", force: :cascade do |t|
    t.integer "player_id"
    t.integer "training_session_id"
    t.string "reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vaccations", force: :cascade do |t|
    t.integer "player_id"
    t.datetime "from_date"
    t.datetime "until_date"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "withdraws", force: :cascade do |t|
    t.datetime "date"
    t.string "description"
    t.decimal "amount", precision: 8, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
