class AddCardsToPlayers < ActiveRecord::Migration[5.1]
  def change
    add_column :players, :cards, :integer
  end
end
