class CreateCards < ActiveRecord::Migration[5.1]
  def change
    create_table :cards do |t|
      t.integer :player_id
      t.datetime :date

      t.timestamps
    end
  end
end
