class AddAwayGameToDeposit < ActiveRecord::Migration[5.1]
  def change
    add_column :deposits, :away_game_id, :integer, default: 0
  end
end
