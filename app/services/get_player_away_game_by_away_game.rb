class GetPlayerAwayGameByAwayGame

  def initialize(away_game_id)
    @away_game_id = away_game_id
  end

  def call
    PlayerAwayGame.where("away_game_id = ?",@away_game_id)
  end

end
