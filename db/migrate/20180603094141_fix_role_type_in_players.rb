class FixRoleTypeInPlayers < ActiveRecord::Migration[5.1]
  def change
    change_column :players, :role, :string
  end
end
