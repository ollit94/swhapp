class Withdraw < ApplicationRecord
  validates :amount, presence: true, numericality: true
  validates :description, presence: true, length: {minimum: 3, maximum: 200}
end
