class FixDateTimeToDateTime < ActiveRecord::Migration[5.1]
  def change
    rename_column :training_sessions, :dateTime, :date_time
  end
end
