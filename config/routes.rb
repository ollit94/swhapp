Rails.application.routes.draw do
  root    'sessions#new'
  get     '/login',                     to: 'sessions#new'
  get     '/player_kassenbericht',      to: 'players#kasse'
  post    '/login',                     to: 'sessions#create'
  delete  '/logout',                    to: 'sessions#destroy'
  get     '/change_password',           to: 'players#change_password'
  patch   '/update_password',           to: 'players#password_update'
  get     '/kassenbericht',             to: 'banks#kassenbericht'
  get     '/player_cards',              to: 'players#player_cards'
  get     '/vaccation_overview',        to: 'vaccations#admin_vaccations'
  get     '/admin_show_vaccation',      to: 'vaccations#admin_show'
  get     '/show_potg',                 to: 'player_of_the_games#show_potg'
  get     '/information',               to: 'static_pages#information'
  post    '/update_player_game',        to: 'away_games#update_player'
  get     'cancellations/new'


  resources :withdraws, except: [:show, :index]
  resources :deposits, except: [:show, :index]
  resources :training_sessions_players, except: [:index, :show]
  resources :training_sessions
  resources :players
  resources :player_penalties, except: [:show]
  resources :penalties, except: [:show]
  resources :cards, except: [:show]
  resources :vaccations, except: [:show]
  resources :player_of_the_games, except: [:show]
  resources :away_games
  resources :weekly_training, only: [:show,:index]
end
