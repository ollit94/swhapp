class NewAwayGame

  def initialize(params)
    @valid_params = params.require(:away_game).permit(:date)
  end

  def call
    AwayGame.new(@valid_params)
  end
  
end
