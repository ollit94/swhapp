module SessionsHelper
  def log_in(player)
    session[:player_id] = player.id
  end

  def current_player
    if (player_id = session[:player_id])
      @current_player ||= Player.find_by(id: player_id)
    elsif (player_id = cookies.signed[:player_id])
      player = Player.find_by(id: player_id)
      if player && player.authenticated?(cookies[:remember_token])
        if player.active
          log_in player
          @current_player = player
        end
      end
    end
  end

  def logged_in?
    !current_player.nil?
  end

  def log_out
    forget(current_player)
    session.delete(:player_id)
    @current_player = nil
  end

  def remember(player)
    player.remember
    cookies.permanent.signed[:player_id] = player.id
    cookies.permanent[:remember_token] = player.remember_token
  end

  def forget(player)
    player.forget
    cookies.delete(:player_id)
    cookies.delete(:remember_token)
  end

  def require_player
    if !logged_in?
      flash[:danger] = "Du musst eingeloggt sein, um diese Aktion ausführen zu können."
      redirect_to root_path
    end
  end

  def require_admin
    if !is_admin?
      flash[:danger] = "Nur der Admin kann diese Aktion ausführen."
      redirect_to root_path
    end
  end

  def require_admin_betreuer_trainer
    if !is_admin? and !is_betreuer? and !is_trainer?
      flash[:danger] = "Du bist nicht berechtigt, diese Aktion ausführen."
      redirect_to root_path
    end
  end

  def require_kassierer_or_admin
    if !is_admin? and !is_kassierer?
      flash[:danger] = "Du bist nicht berechtigt, diese Aktion ausführen."
      redirect_to root_path
    end
  end

  def require_admin_trainer
    if !is_admin? and !is_trainer?
      flash[:danger] = "Du bist nicht berechtigt, diese Aktion ausführen."
      redirect_to root_path
    end
  end

  def require_kassierer_or_admin_or_trainer
    if !is_admin? and !is_kassierer? and !is_trainer?
      flash[:danger] = "Du bist nicht berechtigt, diese Aktion ausführen."
      redirect_to root_path
    end
  end

  def is_kassierer_or_admin?
    if is_admin? or is_kassierer?
      return true
    end
    return false
  end

  def is_kassierer_or_admin_or_trainer?
    if is_admin? or is_kassierer? or is_trainer?
      return true
    end
    return false
  end

  def is_more_than_player?
    if is_admin? or is_trainer? or is_betreuer?
      return true
    end
    return false
  end

  def is_admin_or_trainer?
    if is_admin? or is_trainer?
      return true
    end
    return false
  end

  def is_admin?(p = current_player)
    if logged_in?
      return p.role.at(4)=="1"
    end
    return false
  end

  def is_betreuer?(p = current_player)
    if logged_in?
      return p.role.at(2)=="1"
    end
    return false
  end

  def is_trainer?(p = current_player)
    if logged_in?
      return p.role.at(3)=="1"
    end
    return false
  end

  def is_kassierer?(p = current_player)
    if logged_in?
      return p.role.at(1)=="1"
    end
    return false
  end

  def is_player?(p = current_player)
    if logged_in?
      return p.role.at(0)=="1"
    end
    return false
  end

  def roles
    if is_admin?
      return "Admin"
    elsif is_trainer?
      return "Trainer"
    elsif is_kassierer?
      if is_player?
        return "Spieler - Kassenwart"
      else
        return "Kassenwart"
      end
    elsif is_betreuer?
      return "Betreuer"
    else
      return "Spieler"
    end
  end

end
