class AddPlayerIdToCancellations < ActiveRecord::Migration[5.1]
  def change
    add_column :cancellations, :player_id, :integer
  end
end
