class CreateTrainingSessionsPlayers < ActiveRecord::Migration[5.1]
  def change
    create_table :training_sessions_players do |t|
      t.integer :player_id
      t.integer :training_sessions_id
      t.string :reason

      t.timestamps
    end
  end
end
