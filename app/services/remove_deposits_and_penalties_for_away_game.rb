class RemoveDepositsAndPenaltiesForAwayGame

  def initialize(away_game)
    @away_game = away_game
  end

  def call
    Player.where("active='t'").each do |p|
      @player_away_game = PlayerAwayGame.where("player_id = ? and away_game_id = ?", p.id, @away_game.id).first
      @player = p
      remove_deposit
      remove_penalty_from_player
    end
  end

  def remove_deposit
    deposit = Deposit.where("away_game_id= ? and player_id= ?",@player_away_game.away_game_id,@player_away_game.player_id).first
    if !deposit.nil?
      deposit.try(:destroy)
      @player.update_attribute(:amount, @player.amount-5)
    end
  end

  def remove_penalty_from_player
    player_penalty = get_player_penalty
    if !player_penalty.nil?
      player_penalty.try(:destroy)
      @player.update_attribute(:amount, @player.amount+5)
    end
  end

  def get_player_penalty
    PlayerPenalty.where("away_game_id= ? and player_id= ?",@player_away_game.away_game_id,@player_away_game.player_id).first
  end

end
