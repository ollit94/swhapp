class GetExistingAwayGameOrNil

  def initialize(params)
    @params = params
  end

  def call
    if @params.has_key?(:date)
      date = Date.strptime(@params[:date], "%d-%m-%Y")
      @away_game = AwayGame.where(date: date.all_day).first()
    else
      @away_game = AwayGame.find(@params[:id])
    end
  end

end
