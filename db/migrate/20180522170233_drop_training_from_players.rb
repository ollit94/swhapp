class DropTrainingFromPlayers < ActiveRecord::Migration[5.1]
  def change
    remove_column :players, :training_possible
    remove_column :players, :training_not_present

    add_column :players, :training_possible, :integer, default:0
    add_column :players, :training_not_present, :integer, default:0
  end
end
