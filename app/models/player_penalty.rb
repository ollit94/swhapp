class PlayerPenalty < ApplicationRecord
  belongs_to :penalty
  belongs_to :player

  def get_type
    "Strafe"
  end

  def get_amount
    - self.penalty.costs
  end

  def get_description
    self.penalty.name
  end
end
