class PlayerPenaltiesController < ApplicationController
  before_action :require_kassierer_or_admin
  before_action :set_player_penalty, only: [:edit, :update, :destroy]

  def index
    #@player_penalties = PlayerPenalty.order(date: :desc).paginate(page: params[:page], per_page: 10)
    @player_penalties = PlayerPenalty.order(date: :desc)
  end

  def new
    @player_penalty = PlayerPenalty.new
    @last_penalty = PlayerPenalty.last
  end

  def create
    @player_penalty = PlayerPenalty.new(player_penalties_params)
    if !is_valid_date
      flash[:danger]= "Strafe konnte nicht zugewiesen werden, da es einen Fehler im Datum gibt"
      redirect_to new_player_penalty_path
      return
    end
    if @player_penalty.save
      player = @player_penalty.player
      player.update_attribute(:amount, player.amount-@player_penalty.penalty.costs)
      flash[:success] = "Strafe erfolgreich angelegt"
      redirect_to new_player_penalty_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    old_player = @player_penalty.player
    old_costs = @player_penalty.penalty.costs
    if !is_valid_date
      flash[:danger]= "Strafe konnte nicht geändert werden, da es einen Fehler im Datum gibt"
      redirect_to edit_player_penalty_path(@player_penalty)
      return
    end
    if @player_penalty.update_attributes(player_penalties_params)
      new_costs = @player_penalty.penalty.costs
      new_player = @player_penalty.player
      old_player.update_attribute(:amount, old_player.amount+old_costs)
      new_player.update_attribute(:amount, new_player.amount-new_costs)
      flash[:success] = "Zuweisung erfolgreich geändert"
      redirect_to player_penalties_path
    else
      render 'edit'
    end
  end

  def destroy
    player = @player_penalty.player
    player.update_attribute(:amount, player.amount+@player_penalty.penalty.costs)
    @player_penalty.destroy
    flash[:success] = "Zuweisung erfolgreich gelöscht"
    redirect_to player_penalties_path
  end

  private
    def player_penalties_params
      params.require(:player_penalty).permit(:player_id, :penalty_id, :date)
    end

    def is_valid_date
      begin
        DateTime.parse(params[:player_penalty][:date])
      rescue ArgumentError
        flash[:danger] = "Gib eine gültige Uhrzeit ein"
        return false
      end
      return true
    end

    def set_player_penalty
      @player_penalty = PlayerPenalty.find(params[:id])
    end
end
