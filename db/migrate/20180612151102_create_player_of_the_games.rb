class CreatePlayerOfTheGames < ActiveRecord::Migration[5.1]
  def change
    create_table :player_of_the_games do |t|
      t.integer :player_id
      t.datetime :date
      t.string :gegner
      
      t.timestamps
    end
  end
end
