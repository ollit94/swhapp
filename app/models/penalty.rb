class Penalty < ApplicationRecord
  has_many :player_penalties, dependent: :destroy
  has_many :players, through: :player_penalties
  validates :name, presence: true, length: {minimum: 3, maximum: 50}
  validates :costs, presence: true, numericality: true
end
