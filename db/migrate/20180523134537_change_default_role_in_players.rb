class ChangeDefaultRoleInPlayers < ActiveRecord::Migration[5.1]
  def change
    change_column_default :players, :role, 0
  end
end
