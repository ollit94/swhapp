class CreateVaccations < ActiveRecord::Migration[5.1]
  def change
    create_table :vaccations do |t|
      t.integer :player_id
      t.datetime :from
      t.datetime :until
      t.string :description
      
      t.timestamps
    end
  end
end
