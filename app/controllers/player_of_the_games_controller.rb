class PlayerOfTheGamesController < ApplicationController
  before_action :require_player
  before_action :require_admin_betreuer_trainer, except: [:index, :show_potg]
  before_action :set_potg, only: [:edit, :update, :destroy]

  def index
    @potgs = PlayerOfTheGame.order(date: :desc).paginate(page: params[:page], per_page: 5)
  end

  def new
    @potg = PlayerOfTheGame.new
  end

  def create
    @potg = PlayerOfTheGame.new(potg_params)
    if @potg.save
      player = @potg.player
      player.update_attribute(:num_potg, player.num_potg+1)
      flash[:success] = "Spieler des Spiels erfolgreich angelegt"
      redirect_to player_of_the_games_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    old_player = @potg.player
    if @potg.update_attributes(potg_params)
      new_player = @potg.player
      old_player.update_attribute(:num_potg, old_player.num_potg-1)
      new_player.update_attribute(:num_potg, new_player.num_potg+1)
      flash[:success] = "Spieler des Spiels erfolgreich geändert"
      redirect_to player_of_the_games_path
    else
      render 'edit'
    end
  end

  def destroy
    player = @potg.player
    player.update_attribute(:num_potg, player.num_potg-1)
    @potg.destroy
    flash[:success] = "Spieler des Spiels erfolgreich gelöscht"
    redirect_to player_of_the_games_path
  end

  def show_potg
  end

  private
    def potg_params
      params.require(:player_of_the_game).permit(:player_id, :date, :gegner)
    end

    def set_potg
      @potg = PlayerOfTheGame.find(params[:id])
    end
end
