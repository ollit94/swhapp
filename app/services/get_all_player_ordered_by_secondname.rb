class GetAllPlayerOrderedBySecondname

  def call
    Player.where("active='t'").order(:secondname)
  end

end
