class CreateTrainingSessions < ActiveRecord::Migration[5.1]
  def change
    create_table :training_sessions do |t|
      t.datetime :dateTime

      t.timestamps
    end
  end
end
