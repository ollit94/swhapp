class AddTrainingPresentToPlayers < ActiveRecord::Migration[5.1]
  def change
    add_column :players, :trainingPresent, :integer
    add_column :players, :trainingPossible, :integer
  end
end
