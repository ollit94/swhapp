class CreatePlayers < ActiveRecord::Migration[5.1]
  def change
    create_table :players do |t|
      t.string :firstname
      t.string :secondname
      t.string :username

      t.timestamps
    end
  end
end
