class RenameDatesInVaccations < ActiveRecord::Migration[5.1]
  def change
    rename_column :vaccations, :from, :from_date
    rename_column :vaccations, :until, :until_date
  end
end
