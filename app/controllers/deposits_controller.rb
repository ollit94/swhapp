class DepositsController < ApplicationController
  before_action :require_kassierer_or_admin
  before_action :set_deposit, only: [:edit, :update, :destroy]

  def new
    @deposit = Deposit.new
  end

  def create
    @deposit = Deposit.new(deposit_params)
    if @deposit.save
      player = @deposit.player
      player.update_attribute(:amount, player.amount+@deposit.amount)
      flash[:success] = "Einzahlung erfolgreich angelegt"
      redirect_to new_deposit_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    old_player = @deposit.player
    old_amount = @deposit.amount
    if @deposit.update_attributes(deposit_params)
      new_player = @deposit.player
      new_amount = @deposit.amount
      old_player.update_attribute(:amount, old_player.amount-old_amount)
      new_player.update_attribute(:amount, new_player.amount+new_amount)
      flash[:success] = "Einzahlung erfolgreich geändert"
      redirect_to kassenbericht_path
    else
      render 'edit'
    end
  end

  def destroy
    player = @deposit.player
    player.update_attribute(:amount, player.amount-@deposit.amount)
    @deposit.destroy
    flash[:success] = "Einzahlung erfolgreich gelöscht"
    redirect_to kassenbericht_path
  end

  private
    def deposit_params
      params.require(:deposit).permit(:player_id,:date,:amount)
    end

    def set_deposit
      @deposit = Deposit.find(params[:id])
    end
end
