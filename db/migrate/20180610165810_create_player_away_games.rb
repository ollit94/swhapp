class CreatePlayerAwayGames < ActiveRecord::Migration[5.1]
  def change
    create_table :player_away_games do |t|
      t.integer :player_id
      t.integer :away_game_id
      t.integer :status, default: 0
      t.timestamps
    end
  end
end
