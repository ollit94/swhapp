class AddColumToPlayerPenalties < ActiveRecord::Migration[5.1]
  def change
    add_column :player_penalties, :away_game_id, :integer
  end
end
