class PlayerOfTheGame < ApplicationRecord
  belongs_to :player
  validates :gegner, presence: true, length: {minimum: 3, maximum: 50}
end
