class UpdateAwayGameStatusForPlayer

  def initialize(params)
    @status = params[:status]
    @player = Player.find(params[:player_id])
    @away_game_id = params[:away_game_id].to_i
    @player_away_game = PlayerAwayGame.where("player_id = ? and away_game_id = ?", @player.id, @away_game_id).first
  end

  def call
    update_player_status
    if @status == "0"
      add_penalty_to_player
      remove_deposit
    elsif @status == "1"
      add_deposit_to_player
    else
      remove_penalty_from_player
      remove_deposit
    end

    return @player_away_game
  end

  private
    def update_player_status
      @player_away_game.update_attribute(:status, @status)
    end

    def remove_deposit
      deposit = Deposit.where("away_game_id= ? and player_id= ?",@player_away_game.away_game_id,@player_away_game.player_id).first
      if !deposit.nil?
        deposit.try(:destroy)
        @player.update_attribute(:amount, @player.amount-5)
      end
    end

    def add_deposit_to_player
      Deposit.create(away_game_id: @player_away_game.away_game_id, player_id: @player_away_game.player_id, amount: 5, date: @player_away_game.away_game.date)
      @player.update_attribute(:amount, @player.amount+5)
    end

    def add_penalty_to_player
      player_penalty = get_player_penalty
      if player_penalty.nil?
        penalty_id = GetPenaltyForAwayGame.new().call
        PlayerPenalty.create(player_id: @player_away_game.player.id, date: @player_away_game.away_game.date, penalty_id: penalty_id, away_game_id: @player_away_game.away_game_id)
        @player.update_attribute(:amount, @player.amount-5)
      end
    end

    def remove_penalty_from_player
      player_penalty = get_player_penalty
      if !player_penalty.nil?
        player_penalty.try(:destroy)
        @player.update_attribute(:amount, @player.amount+5)
      end
    end

    def get_player_penalty
      PlayerPenalty.where("away_game_id= ? and player_id= ?",@player_away_game.away_game_id,@player_away_game.player_id).first
    end

end
