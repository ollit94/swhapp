class TrainingSessionsPlayer < ApplicationRecord
  belongs_to :player
  belongs_to :training_session
  validates :reason, presence: true, length: {minimum: 3, maximum: 200}

  def get_until
    training_date = Date.parse(self.training_session.date_time.to_s)
    vaccation = Vaccation.where("player_id=? AND from_date <= ? AND until_date >= ?",self.player.id, training_date.end_of_day, training_date.beginning_of_day).first
    if vaccation.nil?
      return training_date
    else
      return vaccation.until_date
    end
  end

  def get_from
    training_date = Date.parse(self.training_session.date_time.to_s)
    vaccation = Vaccation.where("player_id=? AND from_date <= ? AND until_date >= ?",self.player.id, training_date.end_of_day, training_date.beginning_of_day).first
    if vaccation.nil?
      return training_date
    else
      return vaccation.from_date
    end
  end

  def get_reason
    training_date = Date.parse(self.training_session.date_time.to_s)
    vaccation = Vaccation.where("player_id=? AND from_date <= ? AND until_date >= ?",self.player.id, training_date.end_of_day, training_date.beginning_of_day).first
    if vaccation.nil?
      return self.reason
    else
      return vaccation.description
    end
  end

end
