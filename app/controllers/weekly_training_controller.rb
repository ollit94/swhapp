class WeeklyTrainingController < ApplicationController
  before_action :require_player
  before_action :require_admin_trainer
  def index
  end

  def show
    get_training_sessions_by_date
    @count_training_session = @training_sessions.count
    @player = Player.where("role LIKE '1%' and active='t'").order(:secondname)

  end

  private
    def get_training_sessions_by_date
      if params.has_key?(:date)
        date = Date.strptime(params[:date], "%d-%m-%Y")
        @training_sessions = TrainingSession.where("date_time <= ? AND date_time >= ?", date.end_of_week, date.beginning_of_week)
        if @training_sessions.nil?
          if date < Date.today
            flash[:danger]= "In dieser Woche gab es kein Training"
          end
          redirect_to weekly_trainings_path
          return
        end
      end
    end

end
