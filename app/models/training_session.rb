class TrainingSession < ApplicationRecord
  has_many :training_sessions_players, dependent: :destroy
  has_many :players, through: :training_sessions_players
  validates :place, presence: true, length: {minimum: 2, maximum: 30}
end
