class AddColumnsToPlayerPenalties < ActiveRecord::Migration[5.1]
  def change
    add_column :player_penalties, :player_id, :int
    add_column :player_penalties, :penalty_id, :int
    add_column :player_penalties, :date, :datetime
  end
end
