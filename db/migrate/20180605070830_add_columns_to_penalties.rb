class AddColumnsToPenalties < ActiveRecord::Migration[5.1]
  def change
    add_column :penalties, :name, :string
    add_column :penalties, :costs, :float
  end
end
