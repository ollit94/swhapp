class UpdateCardsNameDefaultForPlayers < ActiveRecord::Migration[5.1]
  def change
    rename_column :players, :cards, :num_cards
    change_column_default :players, :num_cards, 0
  end
end
