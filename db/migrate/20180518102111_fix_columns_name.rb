class FixColumnsName < ActiveRecord::Migration[5.1]
  def change
    rename_column :training_sessions_players, :training_sessions_id, :training_session_id
  end
end
