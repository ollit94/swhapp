module ApplicationHelper

  def mobile_header_title(title="")
    if title.empty?
      "SWH Trainingsapp"
    else
      title
    end
  end

  def display_avatar(user)
    if !user.image.path.nil?
      cl_image_tag("thumb_#{File.basename(user.image.path)}")
    else
      image_tag("thumb_default.png")
    end
  end

end
