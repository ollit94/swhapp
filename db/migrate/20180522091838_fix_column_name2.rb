class FixColumnName2 < ActiveRecord::Migration[5.1]
  def change
    rename_column :players, :trainingPresent, :training_not_present
    rename_column :players, :trainingPossible, :training_possible
  end
end
