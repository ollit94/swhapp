class CreateAwayGames < ActiveRecord::Migration[5.1]
  def change
    create_table :away_games do |t|
      t.datetime :date
      t.timestamps
    end
  end
end
