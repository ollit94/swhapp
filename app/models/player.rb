class Player < ApplicationRecord
  attr_accessor :remember_token, :crop_x, :crop_y, :crop_w, :crop_h
  has_many :training_sessions_players, dependent: :destroy
  has_many :training_sessions, through: :training_sessions_players
  has_many :player_penalties, dependent: :destroy
  has_many :penalties, through: :player_penalties
  has_many :player_away_games, dependent: :destroy
  has_many :vaccations, dependent: :destroy
  has_many :away_games, through: :player_away_games
  has_many :deposits, dependent: :destroy
  has_many :cards, dependent: :destroy
  has_many :player_of_the_games, dependent: :destroy
  validates :username, presence: true, uniqueness: {case_sensitive: false},length: {minimum:2, maximum:51}
  validates :firstname, presence: true, length: {minimum:2, maximum:20}
  validates :secondname, presence: true, length: {minimum:2, maximum:30}
  mount_uploader :image, ImageUploader
  after_update :crop_image

  has_secure_password

  def crop_image
    image.recreate_versions! if crop_x.present?
  end

  # Returns the hash digest of the given string.
  def Player.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def Player.new_token
    SecureRandom.urlsafe_base64
  end

  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = Player.new_token
    update_attribute(:remember_digest, Player.digest(remember_token))
  end

  # Returns true if the given token matches the digest.
  def authenticated?(remember_token)
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end
end
