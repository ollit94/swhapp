class ChangeDefaultTrainingFromPlayers < ActiveRecord::Migration[5.1]
  def change
    change_column_default :players, :training_possible, 0
    change_column_default :players, :training_not_present, 0
  end
end
