class AddAdminToPlayers < ActiveRecord::Migration[5.1]
  def change
    add_column :players, :role, :integer
    change_column_default :players, :role, 0
  end
end
