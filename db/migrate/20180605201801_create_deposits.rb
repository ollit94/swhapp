class CreateDeposits < ActiveRecord::Migration[5.1]
  def change
    create_table :deposits do |t|
      t.integer :player_id
      t.datetime :date
      t.decimal :amount, precision: 8, scale: 2

      t.timestamps
    end
  end
end
