class GetPenaltyForAwayGame

  def initialize()
  end

  def call
    penalty = Penalty.where(name: "Auswärtsspiel").first
    if penalty.nil?
      Penalty.create(name: "Auswärtsspiel", costs: 5).id
    else
      penalty.id
    end
  end

end
