class CardsController < ApplicationController
  before_action :require_admin_betreuer_trainer
  before_action :set_card, only: [:edit, :update, :destroy]

  def index
    @cards = Card.order(date: :desc).paginate(page: params[:cards_page], per_page: 5)
    @players = Player.where("role LIKE '1%' and active='t'").order(num_cards: :desc).paginate(page: params[:players_page], per_page: 5)
  end

  def new
    @card = Card.new
  end

  def create
    @card = Card.new(cards_params)
    if @card.save
      player = @card.player
      player.update_attribute(:num_cards, player.num_cards+1)
      flash[:success] = "Gelbe Karte erfolgreich angelegt"
      redirect_to cards_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    old_player = @card.player
    if @card.update_attributes(cards_params)
      new_player = @card.player
      old_player.update_attribute(:num_cards, old_player.num_cards-1)
      new_player.update_attribute(:num_cards, new_player.num_cards+1)
      flash[:success] = "Gelbe Karte erfolgreich geändert"
      redirect_to cards_path
    else
      render 'edit'
    end
  end

  def destroy
    player = @card.player
    player.update_attribute(:num_cards, player.num_cards-1)
    @card.destroy
    flash[:success] = "Gelbe Karte erfolgreich gelöscht"
    redirect_to cards_path
  end

  private
    def cards_params
      params.require(:card).permit(:player_id,:date)
    end

    def set_card
      @card = Card.find(params[:id])
    end
end
