class Deposit < ApplicationRecord
  belongs_to :player
  validates :amount, presence: true, numericality: true

  def get_type
    "Einzahlung"
  end

  def get_amount
    amount
  end

  def get_description
    "---"
  end

end
