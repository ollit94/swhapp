class GetAwayGameById

  def initialize(params)
    @params = params
  end
  def call
    AwayGame.find(@params[:id])
  end

end
