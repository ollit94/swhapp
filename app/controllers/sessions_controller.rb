class SessionsController < ApplicationController

  def new
    if logged_in?
      redirect_to @current_player
    end
  end

  def create
    player = Player.find_by(username: params[:session][:username].downcase)
    if player && player.authenticate(params[:session][:password])
      if player.active
        log_in player
        remember player
        redirect_to player
      else
        flash.now[:danger] = 'Account nicht aktiv'
        render 'new'
      end
    else
      flash.now[:danger] = 'Falsches Passwort oder falscher Benutzername'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

end
