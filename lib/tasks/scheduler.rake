desc "This task is called by the Heroku scheduler add-on"
task :update_feed => :environment do
  puts "Updating feed..."
  training_session = TrainingSession.where("date_time <= ? AND date_time >= ?",Date.today.end_of_day,Date.today.at_beginning_of_day).first()
  if !training_session.nil?
    fcm_client = FCM.new("AAAAegDU6xc:APA91bFv8vpkb3osxu9yXLCrgess9h_pAk6aWPITUqHU8pIuG8Rwyt5-M0-oGYrfRkDO5qbdr3SXbcVEEnNNfzut-Wd3ESgBSQVf5mGtfYekTwa_zjItf14bXv4_D2AjZywX4HVaxO7CRm6VL6o33ROjQVUAcfJurg")
    options = {
      priority: 'high',
      data: {
        message: "Bist du heute beim Training?"
      },
      notification: {
        title:"Bist du heute beim Training?",
        body: "Heute ist um #{training_session.date_time.to_time.strftime('%H:%M')} Uhr Training in #{training_session.place}. Wenn du nicht da bist, dann melde dich jetzt ab. ",
        sound: 'default'
      }
   }
   user_device_ids=["eUQqUWnxQsc:APA91bE4U0WqxTogn4dGn0apr9gCeMr66w3uVH2nZSVSpyBOvenxeOLoQ7300WrGh1TF5nOCF4aH5kU3JYekXj7uOZoIylCWtyfYS-8kG9FEbinpj92VyWrJhJtoH4F7caGG3thZ7mIhquyDXHfJVHzbjVzWp2MFAg"]
   fcm_client.send_to_topic("allPlayers",options)
  end
  puts "done."
end
