class WithdrawsController < ApplicationController
  before_action :require_kassierer_or_admin
  before_action :set_withdraw, only: [:edit, :update, :destroy]

  def new
    @withdraw = Withdraw.new
  end

  def create
    @withdraw = Withdraw.new(withdraw_params)
    if @withdraw.save
      flash[:success] = "Auszahlung erfolgreich angelegt"
      redirect_to new_withdraw_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @withdraw.update_attributes(withdraw_params)
      flash[:success] = "Auszahlung erfolgreich geändert"
      redirect_to kassenbericht_path
    else
      render 'edit'
    end
  end

  def destroy
    @withdraw.destroy
    flash[:success] = "Auszahlung erfolgreich gelöscht"
    redirect_to kassenbericht_path
  end

  private
    def withdraw_params
      params.require(:withdraw).permit(:description,:date,:amount)
    end

    def set_withdraw
      @withdraw = Withdraw.find(params[:id])
    end

end
