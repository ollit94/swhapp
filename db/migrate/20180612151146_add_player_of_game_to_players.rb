class AddPlayerOfGameToPlayers < ActiveRecord::Migration[5.1]
  def change
    add_column :players, :num_potg, :int, default: 0
  end
end
