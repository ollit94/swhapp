class CreateWithdraws < ActiveRecord::Migration[5.1]
  def change
    create_table :withdraws do |t|
      t.datetime :date
      t.string :description
      t.decimal :amount, precision: 8, scale: 2

      t.timestamps
    end
  end
end
