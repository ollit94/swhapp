class ValidateAwayGame

 def initialize(params)
   @date_string = params[:away_game][:date]
   if is_valid_date
     @date = DateTime.parse(@date_string)
   end
 end

 #check if an away_game at the same day already exists
 def same_day_game
   return !AwayGame.where(date: @date.all_day).first().nil?
 end

 #check if the given date is valid
 def is_valid_date
   begin
     DateTime.parse(@date_string)
   rescue ArgumentError
     return false
   end
   return true
 end

 def validate
  if !is_valid_date
    return 1
  elsif same_day_game
    return 2
  else
    return 0
  end
 end

end
