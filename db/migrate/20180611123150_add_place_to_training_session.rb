class AddPlaceToTrainingSession < ActiveRecord::Migration[5.1]
  def change
    add_column :training_sessions, :place, :string, default: "Hultrop"
  end
end
