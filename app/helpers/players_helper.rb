module PlayersHelper
  def calculate_training_player_percentage(player)
    if is_player?(player)
      calculate_training_percentage(player.training_possible, player.training_not_present)
    else
      "-.- "
    end
  end

  def calculate_training_team_percentage
    training_possible = 0
    training_not_present = 0
    Player.where("role LIKE '1%'").all.each do |p|
      training_possible += p.training_possible
      training_not_present += p.training_not_present
    end
    calculate_training_percentage(training_possible, training_not_present)
  end

  def calculate_training_percentage(possible,not_present)
    if(not_present > possible || possible == 0)
      100
    else
      ((possible - not_present).to_f / possible * 100).round(2)
    end
  end

  def is_player_not_present(player)
    @cancelledPlayer.each do |ps|
      if ps.username == player.username
        return ps
      end
    end
    return nil
  end

end
