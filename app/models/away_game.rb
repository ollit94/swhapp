class AwayGame < ApplicationRecord
  has_many :player_away_games, dependent: :destroy
  has_many :players, through: :player_away_games
end
