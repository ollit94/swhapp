class AwayGamesController < ApplicationController
  before_action :require_kassierer_or_admin
  before_action :set_away_game, only: [:edit, :update, :destroy]
  before_action :validate_away_game, only: [:create, :update]

  def index
    @away_games = GetAllAwayGames.new().call
  end

  def show
    @away_game = GetExistingAwayGameOrNil.new(params).call
    if @away_game.nil?
      redirect_to new_away_game_path(date: params[:date])
      return
    end
    @player_away_game = GetPlayerAwayGameByAwayGame.new(@away_game.id).call
    @players_away_game =  @away_game.players
    @players = GetAllPlayerOrderedBySecondname.new().call
    @count_paid = @players_away_game.where("status !=0").count
  end

  def new
    @away_game = GetNewAwayGame.new().call
  end

  def create
    @away_game = NewAwayGame.new(params).call
    if @away_game.save
      ChargeAllPlayer.new(@away_game).call
      flash[:success] = "Auswärtsspiel erfolgreich angelegt"
      redirect_to @away_game
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @away_game.update_attributes(away_game_params)
      flash[:success] = "Auswärtsspiel erfolgreich geändert"
      redirect_to @away_game
    else
      render 'edit'
    end
  end

  def destroy
    #AddAmountToAllPlayers.new(@away_game).call
    RemoveDepositsAndPenaltiesForAwayGame.new(@away_game).call
    @away_game.destroy
    flash[:success] = "Auswärtsspiel erfolgreich gelöscht"
    redirect_to away_games_path
  end

  def update_player
    player_away_game = UpdateAwayGameStatusForPlayer.new(params).call
    redirect_to player_away_game.away_game
  end

  private
    def set_away_game
      @away_game = GetAwayGameById.new(params).call
    end

    def validate_away_game
        validation_code = ValidateAwayGame.new(params).validate
        if validation_code == 1
          flash[:danger]= "Spiel konnte nicht erstellt werden, da es einen Fehler im Datum oder der Uhrzeit gibt"
          redirect_to new_away_game_path
          return
        elsif validation_code == 2
          flash[:danger] = "Es existiert bereits ein Spiel an diesem Tag"
          redirect_to new_away_game_path
          return
        end
    end

end
