class ChargeAllPlayer

  def initialize(away_game)
    @away_game = away_game
  end

  def call
    penalty_id = GetPenaltyForAwayGame.new().call
    Player.where("active='t'").each do |p|
      PlayerPenalty.create(player_id: p.id, date: @away_game.date, penalty_id: penalty_id, away_game_id: @away_game.id)
      PlayerAwayGame.create(player_id: p.id, away_game_id: @away_game.id, status: 0)
      p.update_attribute(:amount, p.amount-5)
    end
  end

end
