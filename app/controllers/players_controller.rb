class PlayersController < ApplicationController
  before_action :require_player
  before_action :require_admin_trainer, except: [:show, :index, :kasse, :password_update, :change_password]
  before_action :require_same_player_or_admin_trainer, only: [:show]
  before_action :set_player, only: [:edit, :update, :show, :destroy]

  def index
    @players = Player.order(:secondname).paginate(page: params[:page], per_page: 10)
  end

  def show
    @training_sessions = TrainingSession.where("date_time <= ? AND date_time >= ?",Date.today.at_end_of_month.next_month,Date.today.at_beginning_of_month.prev_month)
    @training_sessions_not_present = @training_sessions.joins(:players).where("training_sessions_players.player_id": @player.id)
  end

  def new
    @player = Player.new
  end

  def create
    @player = Player.new(player_params)
    if @player.save
      set_training_possible
      set_roles
      if player_params[:image].present?
        render 'crop'
      elsif player_params[:crop_x].present?
        Cloudinary::Uploader.upload("public#{@player.image.thumb.url}", use_filename: true, unique_filename: false)
        redirect_to @player
      else
        redirect_to @player
      end
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @player.update_attributes(player_params)
      set_roles
      if player_params[:image].present?
        render 'crop'
      elsif player_params[:crop_x].present?
        Cloudinary::Uploader.upload("public#{@player.image.thumb.url}", use_filename: true, unique_filename: false)
        flash[:success] = "Spieler erfolgreich geändert"
        redirect_to players_path
      else
        flash[:success] = "Spieler erfolgreich geändert"
        redirect_to players_path
      end
    else
      render 'edit'
    end
  end

  def destroy
    @player.destroy
    flash[:success] = "Spieler erfolgreich gelöscht"
    redirect_to players_path
  end

  def kasse
    if params.has_key?(:player_id) && is_kassierer_or_admin?
      @player = Player.find(params[:player_id])
    else
      @player = current_player
    end
    @penalties = PlayerPenalty.where("player_id =?",@player.id)
    @deposits = @player.deposits.all
    @kontoverlauf = (@penalties + @deposits).sort_by(&:date).reverse
  end

  def password_update
    @player = current_player
    update_players_password
  end

  def change_password
    @player = current_player
  end

  def player_cards
    @player = current_player
    @cards = @player.cards.paginate(page: params[:page], per_page: 10)
  end

  private
    def set_player
      @player= Player.find(params[:id])
    end

    def password_params
      params.require(:player).permit(:password, :password_confirmation)
    end

    def player_params
      params.require(:player).permit(:firstname,:active,:secondname,:username,:password,:password_confirmation,:image,:crop_x,:crop_y,:crop_w,:crop_h,:cropped)
    end

    def require_same_player_or_admin_trainer
      if logged_in? and params.has_key?(:id)
        if current_player.id != params[:id].to_i and !is_admin? and !is_trainer?
            flash[:danger] = "Du kannst nur deine eigenen Trainingseinheiten ansehen oder du bist admin."
            redirect_to current_player
        end
      else
        flash[:danger] = "Du kannst nur deine eigenen Trainingseinheiten ansehen oder du bist admin."
        redirect_to root_path
      end
    end

    def set_training_possible
        count_possible_training_session = TrainingSession.where("date_time >= ?", @player.created_at).count
        @player.update_attribute(:training_possible,count_possible_training_session)
    end

    def update_players_password
      if !!@player.authenticate(params[:player][:password_old])
        if params[:player][:password].empty?
          flash[:danger] = "Passwort darf nicht leer sein."
          redirect_to change_password_path
        elsif params[:player][:password] != params[:player][:password_confirmation]
          flash[:danger] = "Das neue Passwort stimmt nicht mit der Wiederholung überein."
          redirect_to change_password_path
        elsif @player.update_attributes(password_params)
          flash[:success] = "Passwort erfolgreich geändert."
          redirect_to @player
        else
          redirect_to change_password_path
        end
      else
        flash[:danger] = "Altes Passwort ist falsch."
        redirect_to change_password_path
      end
    end

    def set_roles
      if params.has_key?(:post)
        role = ""
        ["1","2","4","8","16"].each do |n|
          if params[:post][:roles].include?(n)
            role << "1"
          else
            role << "0"
          end
        end
        role= "1" if role.empty?
        @player.update_attribute(:role,role)
      end
    end
end
