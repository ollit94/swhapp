class ChangeDefaultAmountForPlayers < ActiveRecord::Migration[5.1]
  def change
    change_column_default :players, :amount, 0.0
  end
end
