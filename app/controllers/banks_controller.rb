class BanksController < ApplicationController
  before_action :require_kassierer_or_admin

  def kassenbericht
    @deposits = Deposit.order(date: :desc).paginate(page: params[:deposits_page], per_page: 5)
    @withdraws = Withdraw.order(date: :desc).paginate(page: params[:withdraws_page], per_page: 5)
    @deposit_sum = Deposit.sum(:amount)
    @withdraw_sum = Withdraw.sum(:amount)
    @open_penalties_sum = Player.sum(:amount)*(-1)
    @balance = @deposit_sum - @withdraw_sum
  end
  
end
