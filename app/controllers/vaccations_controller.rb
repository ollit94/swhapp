class VaccationsController < ApplicationController
  before_action :require_player
  before_action :set_vaccation, only: [:update, :destroy]
  before_action :does_vaccation_exists, only: [:edit]
  before_action :set_vaccations, only: [:admin_show]
  before_action :dates_are_valid, only:[:create, :update]

  def index
    @vaccations = Vaccation.where("player_id = ?", current_player.id)
  end

  def new
    @vaccation = Vaccation.new
  end

  def create
    @vaccation = Vaccation.new(vaccation_params)
    if DateTime.now >= @vaccation.from_date
      flash[:danger] = "Du kannst dich nicht mehr abmelden, bitte melde dich für heute bei dem Training über den Trainingskalender ab"
      render 'new'
    else
      if @vaccation.save
        delete_player_from_existing_sessions
        flash[:success] = "Abmeldung erfolgreich angelegt"
        redirect_to vaccations_path
      else
        render 'new'
      end
    end
  end

  def edit
  end

  def update
    if DateTime.now >= @vaccation.from_date
      flash[:danger] = "Du kannst dich nicht mehr abmelden, bitte melde dich für heute bei dem Training über den Trainingskalender ab"
      redirect_to edit_vaccation_path(@vaccation)
    else
      destroy_all_training_cancellations
      if @vaccation.update_attributes(vaccation_params)
        delete_player_from_existing_sessions
        flash[:success] = "Abmeldung erfolgreich geändert"
        redirect_to vaccations_path
      else
        render 'edit'
      end
    end
  end

  def destroy
    if DateTime.now <= @vaccation.from_date
      destroy_all_training_cancellations
      @vaccation.destroy
      flash[:success] = "Abmeldung erfolgreich gelöscht"
      redirect_to vaccations_path
    end
  end

  def admin_vaccations
    @vaccations = Vaccation.all
  end

  def admin_show
  end

  private
    def vaccation_params
      params.require(:vaccation).permit(:from_date,:until_date,:player_id,:description)
    end

    def set_vaccation
      @vaccation = Vaccation.find(params[:id])
    end

    def dates_are_valid
      from = is_valid_date(vaccation_params[:from_date])
      to = is_valid_date(vaccation_params[:until_date])
      if from.nil? or to.nil?
        flash[:danger] = "Das Datumformat stimmt nicht"
        redirect_to new_vaccation_path
        return
      end
      if from > to
        flash[:danger] = "Das vom Datum darf nicht größer sein als das bis Datum"
        redirect_to new_vaccation_path
        return
      end
    end

    def is_valid_date(date_string)
      date = nil
      begin
        date = DateTime.parse(date_string)
      rescue ArgumentError
        flash[:danger] = "Gib eine gültige Uhrzeit ein"
        return nil
      end
      return date
    end

    def set_vaccations
      if params.has_key?(:date)
        date = Date.strptime(params[:date], "%d-%m-%Y")
        @abmeldungen = []
        Player.where("role LIKE '1%'").all.each do |p|
          @vaccation = Vaccation.where("player_id=? AND from_date <= ? AND until_date >= ?",p.id, date.end_of_day, date.beginning_of_day).first
          if !@vaccation.nil?
            @abmeldungen.push(@vaccation)
          else
            @training_session = TrainingSession.where(date_time: date.all_day).first()
            if !@training_session.nil?
              @cancellation = TrainingSessionsPlayer.where("player_id=? AND training_session_id=?",p.id,@training_session.id).first
              if !@cancellation.nil?
                @abmeldungen.push(@cancellation)
              end
            end
          end
        end
        if !@abmeldungen.any?
          if date <= Date.today
            flash[:danger]= "An diesem Tag hatte keiner Urlaub eingetragen"
          else
            flash[:danger]= "An diesem Tag hat keiner Urlaub eingetragen"
          end
          redirect_to vaccation_overview_path
          return
        end
        return @abmeldungen
      else
        flash[:danger]= "Fehler: kein Datum übergeben"
        redirect_to vaccation_overview_path
      end
    end

    def delete_player_from_existing_sessions
      TrainingSession.where("date_time >= ? and date_time <= ?", @vaccation.from_date.beginning_of_day, @vaccation.until_date.end_of_day).each do |t|
        if TrainingSessionsPlayer.where("player_id= ? and training_session_id = ?",current_player.id,t.id).first.nil?
          TrainingSessionsPlayer.create(player_id: current_player.id, training_session_id: t.id, reason: @vaccation.description)
          current_player.update_attribute(:training_not_present,current_player.training_not_present+1)
        end
      end
    end

    def destroy_all_training_cancellations
      current_player.training_sessions.where("date_time >= ? and date_time <= ?", @vaccation.from_date.beginning_of_day, @vaccation.until_date.end_of_day).each do |t|
        training_session_player = TrainingSessionsPlayer.where("player_id = ? and training_session_id = ?",current_player.id, t.id)
        if !training_session_player.nil?
          TrainingSessionsPlayer.delete(training_session_player)
          current_player.update_attribute(:training_not_present,current_player.training_not_present-1)
        end
      end
    end

    def does_vaccation_exists
      if params.has_key?(:date)
        date = Date.strptime(params[:date], "%d-%m-%Y")
        @vaccation = Vaccation.where("from_date <= ? AND until_date >= ? AND player_id=?", date.end_of_day, date.beginning_of_day, current_player.id).first()
        if @vaccation.nil?
          if date < Date.today
            flash[:danger]= "An diesem Tag hattest du dich nicht abgemeldet"
          else
            flash[:danger]= "An diesem Tag hast du dich noch nicht abgemeldet"
          end
          redirect_to vaccations_path
          return
        end
      else
        @vaccation = Vaccation.find(params[:id])
      end
    end

end
