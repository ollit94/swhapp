class TrainingSessionsPlayersController < ApplicationController
  before_action :require_player
  before_action :set_player_and_training_session
  before_action :require_same_player_or_admin_trainer
  before_action :set_training_sessions_player, only: [:edit, :update]

  def new
    @training_session_player = TrainingSessionsPlayer.new
  end

  def create
    @training_session_player = TrainingSessionsPlayer.new(training_sessions_player_params)
    if is_before_training_or_admin && @training_session_player.save
      @player.update_attribute(:training_not_present, @player.training_not_present + 1)
      flash[:success] = "Erfolgreich für das Training abgemeldet"
      redirect_to @training_session
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if is_before_training_or_admin
      if @training_session_player.update_attributes(training_sessions_player_params)
        flash[:success] = "Grund erfolgreich geändert"
        redirect_to @training_session
      else
        flash[:danger] = "Der Grund muss mindestens 2 und darf maximal 200 Wörter lang sein"
        redirect_to edit_training_sessions_player_path(@training_session_player.id,player_id: @player.id,training_session_id:@training_session.id)
      end
    else
      redirect_to @training_session
    end
  end

  def destroy
    if is_before_training_or_admin
      TrainingSessionsPlayer.find(params[:id]).destroy
      @player.update_attribute(:training_not_present, @player.training_not_present - 1)
      flash[:success] = "Wieder für das Training angemeldet"
      redirect_to @training_session
    else
      redirect_to @training_session
    end
  end

  private
    def training_sessions_player_params
      params.require(:training_sessions_player).permit(:player_id, :training_session_id, :reason)
    end

    def set_player_and_training_session
      if params.has_key?(:training_sessions_player)
        @training_session = TrainingSession.find(training_sessions_player_params[:training_session_id])
        @player = Player.find(training_sessions_player_params[:player_id])
      else
        @training_session = TrainingSession.find(params[:training_session_id])
        @player = Player.find(params[:player_id])
      end
    end

    def set_training_sessions_player
      @training_session_player = TrainingSessionsPlayer.find(params[:id])
    end

    def require_same_player_or_admin_trainer
      if current_player != @player and !is_admin? and !is_betreuer? and !is_trainer?
          flash[:danger] = "Du kannst nur deine eigenen Trainingseinheiten bearbeiten."
          redirect_to root_path
      end
    end

    def is_before_training_or_admin
      if !is_more_than_player?
        if @training_session.date_time - 2.hours < DateTime.now
          flash[:danger] = "Du kannst dich nicht mehr vom Training abmelden oder die Abmeldung bearbeiten."
          return false
        end
      end
      return true
    end

end
