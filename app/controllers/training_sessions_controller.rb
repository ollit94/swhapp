class TrainingSessionsController < ApplicationController
  before_action :require_player
  before_action :require_admin_trainer, except: [:show, :index]
  before_action :does_training_exist, only: [:show]
  before_action :set_training_session, only: [:update, :edit, :destroy]
  before_action :is_finished_and_not_more_than_player, only: [:show]

  def index
    if is_more_than_player? and params.has_key?(:player_id)
      @player = Player.find(params[:player_id])
      @current= false
    else
      @player = current_player
      @current= true
    end
    @training_sessions = TrainingSession.all
    @training_sessions_not_present = TrainingSessionsPlayer.where(player_id: @player.id)
  end

  def show
    @player = Player.where("created_at <= ? and role LIKE '1%' and active='t'", @training_session.date_time).order(:secondname)
    @count_available = @player.count - @training_session.players.where("role LIKE '1%' and active='t'").count
  end

  def new
    @training_session = TrainingSession.new
    @all_training_sessions = TrainingSession.all
  end

  def create
    @training_session = TrainingSession.new(trainingSession_params)
    if !is_valid_date
      flash[:danger]= "Training konnte nicht erstellt werden, da es einen Fehler im Datum oder der Uhrzeit gibt"
      redirect_to new_training_session_path
      return
    end
    if same_day_training
      flash[:danger] = "Es existiert bereits ein Training an diesem Tag"
      redirect_to new_training_session_path
      return
    end
    if @training_session.save
      add_training_all_Players
      delete_vaccation_players_from_session
      flash[:success]= "Training erfolgreich erstellt"
      redirect_to @training_session
    else
      flash[:danger]= "Training konnte nicht erstellt werden"
      @all_training_sessions = TrainingSession.all
      render 'new'
    end
  end

  def edit
  end

  def update
    old_date = @training_session.date_time
    if is_valid_date && @training_session.update_attributes(trainingSession_params)
      update_possible_training_player(old_date)
      flash[:success] = "Uhrzeit erfolgreich geändert"
      redirect_to @training_session
    else
      flash[:danger] = "Fehler in der Uhrzeit, bitte gib eine gültige Uhrzeit an"
      render 'edit'
    end
  end

  def destroy
    delete_training
    @training_session.destroy
    flash[:success] = "Training erfolgreich gelöscht"
    redirect_to training_sessions_path
  end


  private
    def set_training_session
      @training_session = TrainingSession.find(params[:id])
    end

    def trainingSession_params
      params.permit(:date_time).merge(params.require(:training_session).permit(:place))
    end

    def add_training_all_Players
      Player.where("created_at <= ? and role LIKE '1%' and active='t'", @training_session.date_time).all.each do |p|
        p.update_attribute(:training_possible,p.training_possible+1)
      end
    end

    def delete_training
      Player.where("created_at <= ? and role LIKE '1%' and active='t'", @training_session.date_time).all.each do |p|
        p.update_attribute(:training_possible,p.training_possible-1)
      end
      @training_session.players.each do |p|
        p.update_attribute(:training_not_present,p.training_not_present-1)
      end
    end

    def same_day_training
      return !TrainingSession.where(date_time: @training_session.date_time.all_day).first().nil?
    end

    def does_training_exist
      if params.has_key?(:date)
        date = Date.strptime(params[:date], "%d-%m-%Y")
        @training_session = TrainingSession.where(date_time: date.all_day).first()
        if @training_session.nil?
          if date < Date.today
            flash[:danger]= "An diesem Tag war kein Training"
          else
            flash[:danger]= "An diesem Tag ist noch kein Training angesetzt"
          end
          redirect_to training_sessions_path
          return
        end
      else
        @training_session = TrainingSession.find(params[:id])
      end
    end

    def is_valid_date
      begin
        DateTime.parse(params[:date_time])
      rescue ArgumentError
        flash[:danger] = "Gib eine gültige Uhrzeit ein"
        return false
      end
      return true
    end

    def update_possible_training_player(old_date)
      Player.where("created_at <= ? and created_at > ?  and role LIKE '1%' and active='t'", @training_session.date_time, old_date).all.each do |p|
        p.update_attribute(:training_possible,p.training_possible+1)
      end
      Player.where("created_at <= ? and created_at > ? and role LIKE '1%' and active='t'", old_date, @training_session.date_time).all.each do |p|
        p.update_attribute(:training_possible,p.training_possible-1)
        player_or_nil = @training_session.players.where("player_id = ?", p.id).select("training_sessions_players.id").first
        if !player_or_nil.nil?
          TrainingSessionsPlayer.find(player_or_nil.id).destroy
          p.update_attribute(:training_not_present,p.training_not_present-1)
        end
      end
    end

    def is_finished_and_not_more_than_player
      @cancelledPlayer = @training_session.players.select(:username, :reason, "training_sessions_players.id","training_sessions_players.reason")
      @abmeldung_until_date={}
      @cancelledPlayer.each do |c|
        tsp = TrainingSessionsPlayer.find(c.id)
        @abmeldung_until_date[tsp.player.id]=tsp.get_until
      end
      if !training_finished? and !(is_more_than_player?)
        ps= is_player_not_present(current_player)
        if ps.nil?
          redirect_to new_training_sessions_player_path(player_id: current_player.id,training_session_id:@training_session.id)
        else
          redirect_to edit_training_sessions_player_path(ps.id, player_id: current_player.id,training_session_id:@training_session.id)
        end
      end
    end

    def training_finished?
      @training_session.date_time < DateTime.now
    end

    def is_player_not_present(player)
      @cancelledPlayer.each do |ps|
        if ps.username == player.username
          return ps
        end
      end
      return nil
    end

    def delete_vaccation_players_from_session
      Vaccation.where("from_date <= ? and until_date >= ?", @training_session.date_time, @training_session.date_time).each do |v|
        TrainingSessionsPlayer.create(player_id: v.player_id, training_session_id: @training_session.id, reason: v.description)
        v.player.update_attribute(:training_not_present,v.player.training_not_present+1)
      end
    end

end
